package worms.model;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Random;

import org.junit.Before;
import org.junit.Test;

import worms.gui.Level;

public class TeamTest {

	
	Team team;
	World world;
	
	@Before
	public void setUp() throws Exception {
		team = new Team("Team");
		Level level = Level.getAvailableLevels()[0];
		level.load();
		world = new World(level.getWorldWidth(), level.getWorldHeight(), level.getPassableMap(), new Random());
	}

	@Test
	public void constructorTest() {
		Team team = new Team("Team");
		assertEquals(team.getName(), "Team");
		assertEquals(team.getLiveWorms().size(), 0);
	}
	
	@Test
	public void addWormTest() {
		Worm tim = new Worm(0, 0, 0, 1, "Tim", world);
		team.addWorm(tim);
		assertEquals(team.getLiveWorms().size(), 1);
		assertEquals(((ArrayList<Worm>) team.getLiveWorms()).get(0), tim);
	}

}
