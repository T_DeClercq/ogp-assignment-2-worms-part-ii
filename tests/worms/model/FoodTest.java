package worms.model;

import static org.junit.Assert.*;

import org.junit.Test;

import worms.util.Util;

public class FoodTest {

	private static final double EPS = Util.DEFAULT_EPSILON;

	@Test
	public void constructorTest() {
		Food food = new Food(1, 2);
		assertEquals(food.getXCoordinate(), 1, EPS);
		assertEquals(food.getYCoordinate(), 2, EPS);
		assertEquals(food.getRadius(), Food.getStandardRadius(), EPS);
	}

}
