package worms.util;

import worms.model.GameObject;

public class Calculate {
	
	
	/**
	 * Returns the distance between two points.
	 */
	public static double distanceBetween(double x1, double y1, double x2, double y2) {
		return Math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
	}

	/**
	 * Returns a boolean indicating if two gameobjects overlap or not.
	 */
	public static boolean overlaps(GameObject o1, GameObject o2) {
		return distanceBetween(o1.getXCoordinate(), o1.getYCoordinate(), o2.getXCoordinate(), o2.getYCoordinate()) <= o1.getRadius() + o2.getRadius();
	}
	
	/**
	 * Returns a boolean indicating if two gameobjects overlap or not.
	 */
	public static boolean overlaps(GameObject o1, double x2, double y2, double radius2) {
		return overlaps(o1.getXCoordinate(), o1.getYCoordinate(), o1.getRadius(), x2, y2, radius2);
	}
	
	/**
	 * Returns a boolean indicating if two gameobjects overlap or not.
	 */
	public static boolean overlaps(double x1, double y1, double radius1, double x2, double y2, double radius2) {
		return distanceBetween(x1, y1, x2, y2) <= radius1 + radius2;
	}
}
