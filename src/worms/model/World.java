package worms.model;

import be.kuleuven.cs.som.annotate.*;

import java.util.ArrayList;
import java.util.Random;

import worms.util.Util;

/**
 * @invar	A worlds dimensions will be valid dimensions.
 * 			| isValidDimensions(this.getWidth(), this.getHeight())
 * @invar	If a game hasn't started yet, no projectile will exist in the world.
 * 			| if !gameStarted then
 * 			|	this.getActiveProjectile() == null
 * @invar	A world may not contain duplicate items.
 * 			| for (object1:this.getAllObjects())
 * 			|	for (object2:this.getAllObjects()\{object1})
 * 			|		!object1.equals(object2)
 * @author 	Thomas De Clercq & Jesse Gielen - 1BA Informatica
 * @gitrepo	https://bitbucket.org/T_DeClercq/ogp-assignment-2-worms-part-ii
 * @version	1.1
 */
public class World {
	
	/**
	 * Create a new world with the given width, height, collisionMap and a seeded random generator.
	 * @param width	The width of the world.
	 * @param height The height of the world.
	 * @param passableMap The passable map for the world.
	 * @param random A seeded random generator.
	 */
	public World(double width, double height, boolean[][] passableMap, Random random)
			throws IllegalArgumentException {
		
		if (!isValidDimension(width, height))
			throw new IllegalArgumentException("World dimensions are Illegal: (" + width + "; " + height + ")");
		
		this.width = width;
		this.height = height;
		
		this.pixelsPerMeter = passableMap.length / height;
		assert Util.fuzzyEquals(pixelsPerMeter, passableMap[0].length / width);
		
		// Flip the passableMap vertically for easier use, top row will be lowest y coordinates.
		int nrRows = passableMap.length;
		this.passableMap = new boolean[nrRows][passableMap[0].length];
		for (int row = 0; row < passableMap.length; row++)
			this.passableMap[nrRows - 1 - row] = passableMap[row];
		
		this.random = random;
	}
	
	private Random random;
	
	/**
	 * Returns the width of this world.
	 * @return The width of this world.
	 */
	@Basic
	public double getWidth() {
		return this.width;
	}
	
	/**
	 * @return The amount of pixels on screen in one row.
	 */
	public int getWidthInPixels() {
		return passableMap[0].length;
	}
	
	/**
	 * Returns the height of this world.
	 * @return The height of this world.
	 */
	@Basic
	public double getHeight() {
		return this.height;
	}
	
	/**
	 * @return The amount of pixels across the screen vertically.
	 */
	public int getHeightInPixels() {
		return passableMap.length;
	}
	
	/**
	 * @return An array of doubles with at index 0 half of the width of the world in
	 * 			meters and at index 1 half of the height of the world in meters.
	 * 			| result[0] == this.getWidth() / 2.0
	 * 			| result[1] == this.getHeight() / 2.0
	 */
	public double[] getCentre() {
		double[] result = { this.getWidth() / 2.0, this.getHeight() / 2.0 };
		return result;
	}
	
	/**
	 * Return a boolean indicating if the given width and height are valid.
	 * @param width	The width to check.
	 * @param height The height to check.
	 * @return | (width >= 0 && width <= maxWidth && height >= 0 && height <= maxHeight);
	 */
	public static boolean isValidDimension(double width, double height){
		return (width >= 0 && width <= maxWidth && height >= 0 && height <= maxHeight);
	}
	
	/**
	 * Returns whether or not a given point is whithin the world bounds.
	 * @param 	x	The x coordinate to check.
	 * @param	y	The y coordinate to check.
	 * @return 	True if the given position is within the world, false otherwise.
	 * 			| result == x >= 0 && x <= this.getWidth() 
				|				&& y >= 0 && y <= this.getHeight();
	 */
	public boolean isInWorld(double x, double y) {
		return x >= 0 && x <= this.getWidth()
				&& y >= 0 && y <= this.getHeight();
	}
	
	/**
	 * Returns whether or not a circle with midpoint (x, y) and given radius
	 * is completely whithin the world bounds.
	 * @param 	x	The x coordinate to check.
	 * @param	y	The y coordinate to check.
	 * @param 	radius	The radius of the circle to check.
	 * @return 	True if the given position is within the world, false otherwise.
	 * 			| result == x - radius >= 0 && x + radius <= this.getWidth() 
				|				&& y - radius >= 0 && y + radius <= this.getHeight();
	 */
	public boolean isInWorld(double x, double y, double radius) {
		return x - radius >= 0 && x + radius <= this.getWidth()
				&& y - radius >= 0 && y + radius <= this.getHeight();
	}
	
	/**
	 * Returns whether or not a circle with midpoint (x, y) and given radius
	 * is completely outside the world bounds.
	 * @param 	x	The x coordinate to check.
	 * @param	y	The y coordinate to check.
	 * @param 	radius	The radius of the circle to check.
	 * @return 	True if the given position is within the world, false otherwise.
	 * 			| result == x - radius >= 0 && x + radius <= this.getWidth() 
				|				&& y - radius >= 0 && y + radius <= this.getHeight();
	 */
	public boolean isOutsideWorld(double x, double y, double radius) {
		return x + radius < 0 || x - radius > this.getWidth()
				|| y + radius < 0 || y - radius > this.getHeight();
		// note, this is not completely correct, for objects that are outside of the world
		// this can return false if they are located near a corner of the world 
	}
	
	/**
	 * Check if a pixel is outside of the world or not.
	 * @param row	The row of the pixel to check.
	 * @param col	The column of the pixel to check.
	 * @return	False if the pixel is outside of the world bounds, true otherwise.
	 * 			| result == row < 0 || col < 0 
				|	|| row >= passableMap.length || col >= passableMap[0].length
	 */
	public boolean isPixelOutsideWorld(int row, int col) {
		return row < 0 || col < 0 
				|| row >= passableMap.length || col >= passableMap[0].length;
	}
	
	private final double width;	
	private final double height;
	
	
	// TODO: Question: Should max dimensions be immutable or changeable during runtime?
	/**
	 * Setter for the maximum width of the world, as this may change in the future.
	 * @param newMaxWidth
	 * 			The new maximum width of the world.
	 * @throws IllegalArgumentException
	 * 			newMaxWidth is less than 0 or NaN.
	 * 			| newMaxWidth < 0 || Double.isNaN(newMaxWidth)
	 */
	public static void setMaxWidth(double newMaxWidth) throws IllegalArgumentException {
		if (newMaxWidth < 0 || Double.isNaN(newMaxWidth))
			throw new IllegalArgumentException();
		maxWidth = newMaxWidth;
	}
	/**
	 * Setter for the maximum height of the world, as this may change in the future.
	 * @param newMaxHeight
	 * 			The new maximum height of the world.
	 * @throws IllegalArgumentException
	 * 			newMaxHeight is less than 0 or NaN.
	 * 			| newMaxHeight < 0 || Double.isNaN(newMaxHeight)
	 */
	public static void setMaxHeight(double newMaxHeight) throws IllegalArgumentException {
		if (newMaxHeight < 0 || Double.isNaN(newMaxHeight))
			throw new IllegalArgumentException();
		maxHeight = newMaxHeight;
	}
	
	/**
	 * The maximum width is arbitrary set to Double.MAX_VALUE, but this may change in the future.
	 */
	@Model
	private static double maxWidth = Double.MAX_VALUE;
	
	/**
	 * The maximum height is arbitrary set to Double.MAX_VALUE, but this may change in the future.
	 */
	@Model
	private static double maxHeight = Double.MAX_VALUE;
	//niet zeker of setters nodig zijn, gezien "all worlds will share the same upper bound"
	
	
	//Adding/removing worms and food shall be worked out defensively.	
	

	/**
	 * Create a new worm at the given location, facing the given direction, with the given radius and name.
	 * @param x	The x coordinate for the new worm.
	 * @param y	The y coordinate for the new worm.
	 * @param direction The direction the new worm will be facing (in radians).
	 * @param radius The radius of the new worm (in meters).
	 * @param name The name of the new worm.
	 * @return The created worm.
	 * @throws IllegalArgumentException
	 * 			If the Worm constructor throws an IllegalArgumentException,
	 * 			i.e. one or more arguments are invalid.
	 */
	public Worm createWorm(double x, double y, double direction,
			double radius, String name) throws IllegalArgumentException {		
		Worm worm = new Worm(x, y, direction, radius, name, this);
		return worm;
	}
	
	/**
	 * Returns all worms in the world.
	 * @return	An arraylist containing references to all worms in this world.
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<Worm> getWorms(){
		return (ArrayList<Worm>) worms.clone();
	}
	
	/**
	 * @return The number of worms in this world.
	 * 			| result == this.getWorms().size()
	 */
	public int getNbWorms() {
		return this.getWorms().size();
	}
	
	/**
	 * Returns an arraylist containing all worms that are not in a team.
	 * @return  All worms in the resulting list will be in this worlds list of worms.
	 * 			| this.getWorms().contains(result)
	 * @return	All worms in the resulting list will not be in a team (independent).
	 * 			| for (worm:result)
	 * 			|	!worm.isInTeam()
	 * @return	All worms in this world that are not in a team are in the resulting list.
	 * 			| for (worm:this.getWorms())
	 * 			|	if (!worm.isInTeam())
	 * 			|		result.contains(worm)
	 */
	public ArrayList<Worm> getIndependentWorms() {
		ArrayList<Worm> independentWorms = new ArrayList<>();
		for (Worm worm : this.getWorms())
			if (!worm.isInTeam())
				independentWorms.add(worm);
		
		return independentWorms;
	}
	
	/**
	 * Add a given worm to this world.
	 * @param worm	The worm to add.
	 * @throws IllegalStateException
	 * 			The game hasn't started yet.
	 * @throws IllegalArgumentException
	 * 			The argument is a null reference.
	 * 			| worm == null
	 * @throws IllegalArgumentException
	 * 			The worm is already added to this world.
	 * 			| this.getWorms().contains(worm)
	 * @post	worm will be in this worlds list of worms.
	 * 			| new.getWorms().contains(worm)
	 */
	public void addWorm(Worm worm) throws IllegalArgumentException, IllegalStateException {
		if (gameStarted)
			throw new IllegalStateException("Cannot add food after game has started.");
		if (worm == null || this.getWorms().contains(worm))
			throw new IllegalArgumentException("Cannot add the same worm twice to a world, or add null reference as a worm.");
		if (worm.getWorld() != this)
			throw new IllegalArgumentException("Worm is created for another world.");
		
		Team lastAddedTeam = this.getLastAddedTeam();
		if (lastAddedTeam != null)
			lastAddedTeam.addWorm(worm);
		
		worms.add(worm);
	}
	
	/**
	 * Remove a worm from this world.
	 * @param worm The worm to remove.
	 * @throws IllegalArgumentException
	 * 			If the worm does not exist in this world.
	 * 			| !this.getWorms().contains(worm)
	 * @post	The worm will no longer be in this world.
	 * 			| !this.getWorms().contains(worm)
	 */
	public void removeWorm(Worm worm) throws IllegalArgumentException {
		int index = worms.indexOf(worm);
		if (index == -1) {
			throw new IllegalArgumentException("Given worm does not exist in this world.");
		}
		boolean currentWormRemoved = false;
		if (currentWormIndex > index)
			currentWormIndex--;
		else if (currentWormIndex == index)
			currentWormRemoved = true;
		
		worms.remove(index);
		// if a the worm with the highest index dies during its own turn, the next worm to play will be the first one (index == 0)
		if (currentWormIndex >= this.getNbWorms())
			currentWormIndex = 0;
		if (currentWormRemoved && this.getNbWorms() > 0)
			getCurrentWorm().giveMaxActionPoints();
	}
	
	private ArrayList<Worm> worms = new ArrayList<Worm>();

	
	/**
	 * Create a new food item at the given location.
	 * @param x	The x coordinate of the location for the new food item.
	 * @param y The y coordinate of the location for the new food item.
	 * @throws IllegalArgumentException
	 * 			If the passed location leads to the food item being (partially) outside of the world bounds.
	 * 			| !isInWorld(x, y, Food.RADIUS)
	 * @return The created food item.
	 */
	public Food createFood(double x, double y) throws IllegalArgumentException {
		
		if (!isInWorld(x, y, Food.getStandardRadius()))
			throw new IllegalArgumentException("Passed location leads to food item being (partially) out of world bounds.");
		
		Food food = new Food(x, y);
		return food;
	}
	
	/**
	 * Returns an arraylist containing references to
	 * all food items in this world.
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<Food> getFood(){
		return (ArrayList<Food>) this.foods.clone();
	}
	
	/**
	 * Add the given food item to this world.
	 * If the given food object is already added to this 
	 * world or is a null reference, an exception is thrown.
	 * @param food	The food object to add.
	 * @throws IllegalStateException
	 * 			The game hasn't started yet.
	 * @throws IllegalArgumentException
	 * 			The argument is a null reference.
	 * 			| food == null
	 * @throws IllegalArgumentException
	 * 			food is already in the food list.
	 * 			| this.getFood().contains(food)
	 * @post	food is in this worlds food list.
	 * 			| new.getFood().contains(food)
	 */
	public void addFood(Food food) throws IllegalStateException, IllegalArgumentException {
		if (gameStarted)
			throw new IllegalStateException("Cannot add food after game has started.");
		if (food == null || this.getFood().contains(food))
			throw new IllegalArgumentException("Cannot add the same food twice to a world, or add null reference as a food item.");
		this.foods.add(food);
	}
	
	/**
	 * Remove the given food object from this world.
	 * If the given food object doesn't exist, throws an exception.
	 * @param food	The food item to remove.
	 * @throws IllegalArgumentException
	 * 			If food is not in this worlds food list.
	 * 			| !this.getFood().contains(food)
	 * @post	food will no longer be in the food list.
	 * 			| !new.getFood().contains(food)
	 */
	public void removeFood(Food food) throws IllegalArgumentException {
		int index = this.foods.indexOf(food);
		if (index == -1) {
			throw new IllegalArgumentException("Given food item does not exist in this world.");
		}
		this.foods.remove(index);
	}
	
	private ArrayList<Food> foods = new ArrayList<Food>();

	
	/**
	 * Create a new projectile in this world and return it.
	 * @return | result == new Projectile(x, y, direction, yield, weapon, this)
	 */
	public Projectile createProjectile(double x, double y, double direction,
			int yield, Weapon weapon) throws IllegalArgumentException {		
		Projectile projectile = new Projectile(x, y, direction, yield, weapon, this);
		return projectile;
	}
	
	/**
	 * Returns the active projectile
	 */
	public Projectile getActiveProjectile() {
		return this.projectile;
	}
	
	/**
	 * @param projectile
	 * @throws IllegalStateException
	 * 			If the game hasn't started yet.
	 * @throws IllegalArgumentException
	 * 			If projectile is null, but the active projectile is already null.
	 * 			| projectile == null && this.getActiveProjectile() == null
	 * @throws IllegalArgumentException
	 * 			If neither the active projectile nor projectile is null.
	 * 			| projectile != null && this.getActiveProjectile() != null
	 * @thros IllegalArgumentException
	 * 			If the projectile is not made for this world
	 * 			| projectile != null && projectile.getWorld() != this
	 * @post 	The active projectile will be projectile.
	 * 			| new.getActiveProjectile() == projectile
	 */
	public void setActiveProjectile(Projectile projectile) throws IllegalStateException, IllegalArgumentException {
		if (!gameStarted)
			throw new IllegalStateException("Cannot set the active projectile before the game started.");
		if (projectile == null && this.getActiveProjectile() == null)
			throw new IllegalArgumentException("Cannot set active projectile to null if it already is null.");
		if (projectile != null && this.getActiveProjectile() != null)
			throw new IllegalArgumentException("Cannot give active projectile a non-null value if it is not null.");
		if (projectile != null && projectile.getWorld() != this)
			throw new IllegalArgumentException("Projectile is created for another world.");
		this.projectile = projectile;
	}
	
	/**
	 * Set the active projectile to be the null reference.
	 * Call this when the active projectile is destroyed.
	 * @throws IllegalStateException
	 * 			If there currently is no active projectile
	 * 			| this.getActiveProjectile() == null
	 * @post 	There will be no active projectile.
	 * 		 	| new.getActiveProjectile() == null
	 */
	public void removeActiveProjectile() throws IllegalStateException {
		if (this.getActiveProjectile() == null)
			throw new IllegalStateException("Active projectile is already null.");
		this.setActiveProjectile(null);
	}
	
	private Projectile projectile;
	
	/**
	 * Returns an arraylist containing all worms, food and projectiles in the world.
	 * @return The resulting arraylist contains all worms.
	 * 			| result.contains(this.getWorms())
	 * @return The resulting arraylist contains all food.
	 * 			| result.contains(this.getFood())
	 * @return The resulting arraylist contains the active projectile if there is one.
	 * 			| if (!this. getActiveProjectile() == null)
	 * 			| 	result.contains(this.getActiveProjectile())
	 */
	public ArrayList<GameObject> getAllObjects() {
		ArrayList<GameObject> allObjects = new ArrayList<>();
		allObjects.addAll(this.getWorms());
		allObjects.addAll(this.getFood());
		if (projectile != null)
			allObjects.add(this.getActiveProjectile());
		return allObjects;
	}
	
	
	/**
	 * Returns the name of the winner of the game, or null if there is none.
	 * The winner can be a worm if it's the only one left and it is
	 * not in a team. It can also be a team if every worm that is not on
	 * that team is dead.
	 * @return	The name of the winning worm if there is only one left and it is not in a team.
	 * 			| if (getLiveWorms().size() == 1 && !liveWorms().get(0).isInTeam())
	 * 			|		result == liveWorms().get(0).getName()
	 * @return 	The name of the winning team if every worm not in
	 * 			that team is dead.
	 * 			| if (this.getLiveIndependentWorms.isEmpty() && this.teamsLeft() == 1)
	 * 			| 		result == this.teamsLeft.get(0).getName()
	 * @return  null if there is no winner.
	 * 			| if (getLiveWorms().size() > 1 && 
	 * 			|		(!liveIndependentWorms.isEmpty || this.nbTeamsLeft() != 1)
	 * 			|	result == null
	 */
	public String getWinner() {
		ArrayList<Worm> liveWorms = getWorms();
		if (liveWorms.size() == 1) {
			Worm survivor = liveWorms.get(0);
			if (survivor.getTeamName() != null)
				return survivor.getTeamName();
			return survivor.getName();
		}
		
		if (this.getIndependentWorms().isEmpty() && this.nbTeamsLeft() == 1) {
			for (Team team : this.getTeams()) {
				if (!team.isDestroyed()) {
					return team.getName();
				}
			}
		}
		return null;
	}
	
	/**
	 * Returns a passable location close to the given location.
	 * If the given location is passable, returns it.
	 */
	public double[] getClosePassableLocation(double x, double y, double radius, double maxDistance) {
		if (isPassable(x, y, radius)) {
			double[] result = {x, y};
			return result;
		}
		
		for (double distance = maxDistance / 2.0; distance <= maxDistance; distance *= 2)
		for (double angle = 0; angle < 2 * Math.PI; angle += 0.0175) {
			double expectedX = x + distance * Math.cos(angle);
			double expectedY = y + distance * Math.sin(angle);
		
			if (this.isAdjacent(expectedX, expectedY, radius)) {
				double[] result = {expectedX, expectedY};
				return result;
			}
		}
		
		return null;
	}
	
//	A location is adjacent to impassable terrain
//	if the location itself is passable and the location's distance to an impassable
//	location is smaller than the game object's radius * 0.1. It is suggested to
//	determine such locations by selecting a perimeter location at random, and
//	then iteratively explore terrain in the direction of the centre of the map until
//	a valid initial position is found.
	/**
	 * Returns a random position that is adjacent to an impassable
	 * position for an object with the given radius.
	 * @param radius	The radius for which adjacency will be checked.
	 * @return	The resulting position will be adjacent to an impassable
	 * 			location for the given radius.
	 * 			| this.isAdjacent(result[0], result[1], radius)
	 */
	public double[] getRandomAdjacentPos(double radius) {
		double[] pos = new double[2];
		
		pos[0] = radius + random.nextDouble() * (this.getWidth() - 2 * radius);
		pos[1] = radius + random.nextDouble() * (this.getHeight() - 2 * radius);
		
		double[] centre = this.getCentre();
		double stepSize = radius / 50;
		
		double direction = Math.atan2(centre[1] - pos[1], centre[0] - pos[0]); // the direction from the random point to the centre
		double offsetAngle = Math.PI/2;	// an angle which will be used to get an offset perpendicular on 'direction' by computing it's sine
										// so we don't just check in a straight line but kind of zigzag to find an adjacent position.
		
		while (!this.isAdjacent(pos[0], pos[1], radius)) {
			double perpendicularOffset = Math.sin(offsetAngle); // the offset perpendicular on direction
			double cos = Math.cos(direction);
			double sin = Math.sin(direction);
			double dx = stepSize * cos - 15 * stepSize * perpendicularOffset * sin;
			double dy = stepSize * sin + 15 * stepSize * perpendicularOffset * cos;
			
			pos[0] += dx;
			pos[1] += dy;
						
			offsetAngle += Math.PI / 10;
			offsetAngle %= 2 * Math.PI;
			
			if (Math.abs(direction - Math.atan2(centre[1] - pos[1], centre[0] - pos[0])) > Math.PI) {
				pos[0] = radius + random.nextDouble() * (this.getWidth() - 2 * radius);
				pos[1] = radius + random.nextDouble() * (this.getHeight() - 2 * radius);
				
				direction = Math.atan2(centre[1] - pos[1], centre[0] - pos[0]);
			}
				
		}
		
		return pos;
	}
	
	/**
	 * Return a boolean indicating if the given disk is adjacant
	 * to an impassable location. Meaning it is passable, but the
	 * distance between this and an impassable location is less
	 * than radius * 0.1
	 * @param x	The x coordinate of the center of the location to check.
	 * @param y	The y coordinate of the center of the location to check.
	 * @param radius	The radius to check for.
	 * @return	| result == this.isPassable(x, y, radius) && !this.isPassable(x, y, radius * 1.1)
	 */
	public boolean isAdjacent(double x, double y, double radius) {
		return this.isPassable(x, y, radius) && !this.isPassable(x, y, radius * 1.1);
	}
	
	/**
	 * Return a boolean indicating if the given location with the given radius
	 * is a passable location or not.
	 * @param x	The x coordinate of the center of the location to check.
	 * @param y	The y coordinate of the center of the location to check.
	 * @param radius	The radius to check for.
	 * @return	True if every pixel contained by the circle with midpoint (x, y) and radius 'radius' is passable.
	 * 			Parts of the circle that are not inside the world are considered passable.
	 * 			| result == for (each x, y in Circle(M = (x0,y0); R = radius))
	 * 			|				passableMap[floor(x)][floor(y)] == true;
	 */
	public boolean isPassable(double x, double y, double radius)
	{
		// Throw an exception or return false?
		// - return false -> isAdjacent will return true if object is adjacent to world bounds!
		//		-> worms should be able to fall out of worlds, so we'll consider parts out of the world to be passable.		
		//		To do this we can just make sure min and max col and row are whithin the bounds of the passablemap array.
		//		This way parts outside the world are just ignored.
		int minCol = (int)Math.floor(toPixels(x - radius));
		if (minCol < 0)
			minCol = 0;
		
		int maxCol = (int)Math.floor(toPixels(x + radius) - 0.00001);
		if (maxCol >= passableMap[0].length)
			maxCol = passableMap[0].length - 1;
		
		int minRow = (int)Math.floor(toPixels(y - radius));
		if (minRow < 0)
			minRow = 0;
		
		int maxRow = (int)Math.floor(toPixels(y + radius) - 0.00001);
		if (maxRow >= passableMap.length)
			maxRow = passableMap.length - 1;
		
		final double radiusSq = radius * radius;
		
		for (int rowIndex = minRow; rowIndex <= maxRow; rowIndex++) {
			for (int colIndex = minCol; colIndex <= maxCol; colIndex++) {
				// makes more sense to do the if's the other way around, but this is faster
				if (!passableMap[rowIndex][colIndex]) {
					if (distToPixelSq(x, y, rowIndex, colIndex) <= radiusSq) // so if the pixel is (partially) whithin the given circle
						return false;
				}
			}
		}
		
		return true;
	}
	
	/**
	 * Get the distance from the given location to the specified pixel.
	 * @param x	The x coordinate of the location.
	 * @param y	The y coordinate of the location.
	 * @param pixelRow The row of the pixel.
	 * @param pixelCol The column o the pixel.
	 * @return The distance from the given point to the pixel.
	 */
	public double distToPixelSq(double x, double y, int pixelRow, int pixelCol) 
	{
		double xDist;
		double yDist;
		
		if (x < toMeters(pixelCol))
			xDist = toMeters(pixelCol) - x;
		else if (x > toMeters(pixelCol+ 1))
			xDist = x - toMeters(pixelCol + 1);
		else
			xDist = 0;
		
		if (y < toMeters(pixelRow))
			yDist = toMeters(pixelRow) - y;
		else if (y > toMeters(pixelRow + 1))
			yDist = y - toMeters(pixelRow + 1);
		else
			yDist = 0;
		
		return xDist * xDist + yDist * yDist;
		
	}
	
	/**
	 * Return a boolean indicating if the given location with the given radius
	 * is an impassable location or not.
	 * @return	The inversion of this.isPassable().
	 * 			| result == !this.isPassable(x, y, radius)
	 */
	public boolean isImpassable(double x, double y, double radius) {
		return !isPassable(x, y, radius);
	}
	
	private boolean[][] passableMap;

	
	/**
	 * Convert meters to pixels.
	 * @param meters	The value in meters to convert to pixels.
	 * @return	The amount of pixels in 'meters' (not rounded).
	 */
	public double toPixels(double meters) {
		return meters * pixelsPerMeter;
	}
	
	/**
	 * Convert pixels to meters.
	 * @param pixels	The value in pixels to convert to meters.
	 * @return	The amount of meters in 'pixels'.
	 */
	public double toMeters(double pixels) {
		return pixels / pixelsPerMeter;
	}
	
	private double pixelsPerMeter;

	
	/**
	 * Return true if the game is finished.
	 * A game is finished when only 1 or 0 worms are alive or all live worms
	 * are in the same team.
	 * @return	| result == getLiveWorms().isEmpty() || getWinner() != null
	 */
	public boolean isGameFinished() {
		return getWorms().isEmpty() || getWinner() != null;
	}
	
	/**
	 * Start the game.
	 */
	public void startGame() throws IllegalStateException {
		if (this.getNbWorms() == 0)
			throw new IllegalStateException("No worms in the world.");
		gameStarted = true;
	}
	
	private boolean gameStarted = false;
	
	
	/**
	 * 
	 * @throws IllegalStateException
	 * 			If the game hasn't started yet.
	 * @throws IllegalStateException	
	 * 			If there is still an active projectile in the world.
	 * 			| this.getActiveProjectile() == null
	 */
	public void startNextTurn() throws IllegalStateException {
		if (!gameStarted)
			throw new IllegalStateException("The game hasn't started yet.");
		if (this.getActiveProjectile() == null) // if there is still an active projectile in the world, don't start next turn yet.
		{
			currentWormIndex++;
			if (currentWormIndex >= this.getWorms().size())
				currentWormIndex = 0;
			this.getCurrentWorm().giveMaxActionPoints();	// restore the current worms action points
			this.getCurrentWorm().hurt(-10);				// also restore 10 hitpoints
		}
	}
	
	/**
	 * Returns the worm whose turn it is or null if the game hasn't started.
	 */
	public Worm getCurrentWorm() {
		if (!this.gameStarted)
			return null;
		return this.getWorms().get(currentWormIndex);
	}
	
	private int currentWormIndex = 0;
	
	/**
	 * Add an empty team to the world with the given name.
	 * @param teamName	The name of the new team.
	 * @throws IllegalArgumentException
	 * 			If the name is not a valid teamname.
	 * 			| !Team.isValidName(teamName)
	 * @throws IllegalArgumentException
	 * 			If there is already a team with the given name.
	 * 			
	 */
	public void addEmptyTeam(String teamName) throws IllegalArgumentException {
		for (Team team:teams)
			if (team.getName() == teamName)
				throw new IllegalArgumentException("Team name is already taken.");
		teams.add(new Team(teamName));
	}
	
	/**
	 * @return	An arraylist of all teams in this world.
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<Team> getTeams() {
		return (ArrayList<Team>) teams.clone();
	}
	
	public Team getLastAddedTeam() {
		if (teams.isEmpty())
			return null;
		return teams.get(teams.size() - 1);
	}
	
	/**
	 * Returns an arraylist containing all teams that are not destroyed.
	 */
	public ArrayList<Team> teamsLeft() {
		ArrayList<Team> teamsLeft = new ArrayList<>();
		for (Team team:this.getTeams())
			if (!team.isDestroyed())
				teamsLeft.add(team);
		
		return teamsLeft;
	}
	
	/**
	 * Return the number of teams that are not destroyed.
	 * @return | count(team:this.getTeams() where !team.isDestroyed())
	 */
	public int nbTeamsLeft() {
		int counter = 0;
		for (Team team:this.getTeams())
			if (!team.isDestroyed())
				counter++;
		return counter;
	}
	
	private ArrayList<Team> teams = new ArrayList<>();
	
	
}
