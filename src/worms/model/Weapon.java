package worms.model;

import be.kuleuven.cs.som.annotate.*;

/**
 * An enumeration of the weapons that exist in the game.
 * Each weapon specifies a projectile mass, projectile damage,
 * cost to fire and minimum and maximum launch force.
 * @author 	Thomas De Clercq & Jesse Gielen - 1BA Informatica
 * @gitrepo	https://bitbucket.org/T_DeClercq/ogp-assignment-2-worms-part-ii
 * @version	1.1
 */
public enum Weapon {
	RIFLE {

		@Override
		public double getMass() {
			return rifleMass;
		}

		@Override
		public int getFireCost() {
			return rifleCost;
		}

		@Override
		public double getMinForce() {
			return rifleForce;
		}

		@Override
		public double getMaxForce() {
			return rifleForce;
		}

		@Override
		public double getDamage() {
			return rifleDamage;
		}
		
	},
	BAZOOKA {

		@Override
		public double getMass() {
			return bazookaMass;
		}

		@Override
		public int getFireCost() {
			return bazookaCost;
		}

		@Override
		public double getMinForce() {
			return bazookaMinForce;
		}

		@Override
		public double getMaxForce() {
			return bazookaMaxForce;
		}

		@Override
		public double getDamage() {
			return bazookaDamage;
		}
		
	};
	
	/**
	 * @return The mass (in kg) of projectiles fired by this weapon.
	 */
	@Basic @Immutable
	public abstract double getMass();
	/**
	 * @return The cost in action points to launch a projectile with this weapon.
	 */
	@Basic @Immutable
	public abstract int getFireCost();
	/**
	 * @return The minimum force at which a projectile from this weapon gets launched.
	 */
	@Basic @Immutable
	public abstract double getMinForce();
	/**
	 * @return The maximum force at which a projectile from this weapon gets launched.
	 */
	@Basic @Immutable
	public abstract double getMaxForce();
	/**
	 * @return The damage in hitpoints which this weapons projectiles deal upon impact.
	 */
	@Basic @Immutable
	public abstract double getDamage();
	
	/**
	 * @return The radius of projectiles fired by this weapon.
	 */
	@Immutable
	public double getRadius() {
		double volume = getMass() / projectileDensity;
		double radius = Math.pow(0.75 * volume / Math.PI, 1/3.0);
		return radius;
	}
	
	private static final double projectileDensity =  7800.0;
	
	private static final double bazookaMass = 0.3;
	private static final double rifleMass = 0.010;
	
	private static final double bazookaMinForce = 2.5;
	private static final double bazookaMaxForce = 9.5;
	private static final double rifleForce = 1.5;
	
	private static final double bazookaDamage = 80.0;
	private static final double rifleDamage = 20.0;
	
	private static final int bazookaCost = 50;
	private static final int rifleCost = 10;
	
	
}
