package worms.model;

import be.kuleuven.cs.som.annotate.*;

public abstract class GameObject {

	/**
	 * Create an object with the given coordinates and radius.
	 * @effect	Set the x-coordinate of this object to xCoordinate.
	 * 			| this.setXCoordinate(xCoordinate)
	 * @effect	Set the y-coordinate of this object to yCoordinate.
	 * 			| this.setYCoordinate(yCoordinate)
	 * @effect 	Set the radius of this object to radius.
	 * 			| this.setRadius(radius)
	 * @param xCoordinate The x-coordinate of the object
	 * @param yCoordinate The y-coordinate of the object
	 * @param radius	The radius of the object.
	 * @throws IllegalArgumentException	
	 * 				When the coordinates or the radius are not valid.
	 * 				| !isValidCoordinate(xCoordinate) || !isValidCoordinate(yCoordinate)
	 * 				|	|| !isValidRadius(radius)
	 * 
	 */
	public GameObject(double xCoordinate, double yCoordinate, double radius) throws IllegalArgumentException {
		this.setXCoordinate(xCoordinate);
		this.setYCoordinate(yCoordinate);
		this.setRadius(radius);
	}
	
	// Aspects related to position: Defensively

	/**
	 * Get the x coordinate of this object.
	 * @return	The x coordinate of this object.
	 */
	@Basic @Raw
	public double getXCoordinate() {
		return this.xCoordinate;
	}
	
	/**
	 * @post 	Set the xCoordinate of this object to be xCoordinate
	 * 		 	| new.getXCoordinate() == xCoordinate
	 * @param 	xCoordinate The new xCoordinate for this object.
	 * @throws	IllegalArgumentException	
	 * 			When xCoordinate is not valid.
	 * 			| !isValidXCoordinate(xCoordinate)
	 */
	@Raw @Model
	protected void setXCoordinate(double xCoordinate) throws IllegalArgumentException {
		if (!isValidXCoordinate(xCoordinate))
			throw new IllegalArgumentException();
		
		this.xCoordinate = xCoordinate;
	}
	
	/**
	 * Get the y coordinate of this object
	 * @return	The y coordinate of this object.
	 */
	@Basic @Raw
	public double getYCoordinate() {
		return this.yCoordinate;
	}
	
	/**
	 * @post 	Set the yCoordinate of this object to be yCoordinate
	 * 		 	| new.getYCoordinate() == yCoordinate
	 * @param yCoordinate The new yCoordinate for this object.
	 * @throws	IllegalArgumentException	
	 * 			When yCoordinate is not valid.
	 * 			| !isValidYCoordinate(yCoordinate)
	 */
	@Raw @Model
	protected void setYCoordinate(double yCoordinate) throws IllegalArgumentException {
		if (!isValidYCoordinate(yCoordinate))
			throw new IllegalArgumentException();
		
		this.yCoordinate = yCoordinate;
	}
	
	
	/**
	 * Check if the passed double is a valid x coordinate.
	 * @param 	coordinate
	 * 			The coordinate to be checked.
	 * @return	A boolean indicating if the given x coordinate is valid.
	 * 			| result == isValidNumber(coordinate)
	 */
	public static boolean isValidXCoordinate(double coordinate) {
		return isValidNumber(coordinate);
	}
	
	/**
	 * Check if the passed double is a valid y coordinate.
	 * @param 	coordinate
	 * 			The coordinate to be checked.
	 * @return	A boolean indicating if the given y coordinate is valid.
	 * 			| result == isValidNumber(coordinate)
	 */
	public static boolean isValidYCoordinate(double coordinate) {
		return isValidNumber(coordinate);
	}
	
	
	private double xCoordinate;
	private double yCoordinate;
	
	// Aspects related to radius: Defensively
	/**
	 * Returns the radius of the object.
	 * @return 	radius
	 * 			The radius of this object in meters.
	 * 			| result == object.radius
	 */
	@Basic @Raw
	public double getRadius() {
		return this.radius;
	}
	
	/**
	 * Sets the radius of the object to the given amount.
	 * @param radius
	 * @throws 	IllegalArgumentException
	 * 			When radius is not valid.
	 * 			| !isValidRadius(radius)
	 */
	@Raw
	public void setRadius(double radius) throws IllegalArgumentException {
		if (!isValidRadius(radius, this))
			throw new IllegalArgumentException();
		
		this.radius = radius;
	}
	
	/**
	 * Check whether a object can have a radius equal to the amount given.
	 * @return	A boolean indicating if radius is a valid radius for the given object.
	 * 			| result == (isValidNumber(radius))
	 */
	public static boolean isValidRadius(double radius, GameObject gameObject) {
		return isValidNumber(radius);
	}

	private double radius;

	
	
	/**
	 * @return	True if the number is valid, false otherwise.
	 * 			| result == !Double.isNaN(number)
	 */
	public static boolean isValidNumber(double number) {
		return !Double.isNaN(number);
	}

}
